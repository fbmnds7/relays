
#-quicklisp
(let ((quicklisp-init #P"/opt/orbi/quicklisp/setup.lisp"))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload :uiop)

(defvar INO "ulisp-esp/ulisp-esp.lisp.ino")

(defvar SRC '("src/ulos.lisp"
              "src/relay.lisp"))

(defvar OUT "ulisp-esp/LispLibrary.h")

(defun src ()
  (mapcan #'uiop:read-file-lines SRC))

(defun simple-quote-line (ln)
  (cond ((string= "" ln) "")
        ((position #\" ln)
         (format nil "\"~a\""
                 (apply #'concatenate 'string
                        (mapcar #'(lambda (c)
                                    (cond ((eql c #\") "\\\"")
                                          ((eql c #\\) "\\\\")
                                          (t (string c))))
                                (coerce ln 'list)))))
        ((position #\\ ln)
         (format nil "\"~a\""
                 (apply #'concatenate 'string
                        (mapcar #'(lambda (c)
                                    (cond ((eql c #\\) "\\\\")
                                          (t (string c))))
                                (coerce ln 'list)))))
        ((eql #\; (aref ln 0)) (format nil "// ~a" ln))
        (t (format nil "\"~a\"" ln))))

(defun write-file-lines (fn seq)
  (with-open-file (f fn :direction :output
                        :if-exists :supersede
                        :if-does-not-exist :create)
    (dolist (ln seq) (format f "~a~%" (simple-quote-line ln)))))

(write-file-lines "/home/dev/projects/relays/ulisp-esp/LispLibrary.h" (src))
;; arduino-cli compile --fqbn "esp8266:esp8266:generic:xtal=160,baud=57600" /home/dev/projects/relays/ulisp-esp/ulisp-esp.ino
;; arduino-cli upload --fqbn "esp8266:esp8266:generic:xtal=160,baud=57600" /home/dev/projects/relays/ulisp-esp/ulisp-esp.ino -p /dev/ttyUSB0
;; curl --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 --retry-all-errors 192.168.178.37/?
;; arduino-cli upload --fqbn "esp8266:esp8266:generic:xtal=160,baud=57600" /home/dev/projects/relays/ulisp-esp/ulisp-esp.ino -p /dev/ttyUSB0
