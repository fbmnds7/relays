; ULOS simple object system

; Define an object
(defun object (&optional parent slots)
  (let ((obj (when parent (list (cons 'parent parent)))))
    (loop
     (when (null slots) (return obj))
     (push (cons (first slots) (second slots)) obj)
     (setq slots (cddr slots)))))

; Get the value of a slot in an object or its parents
(defun value (obj slot)
  (when (symbolp obj) (setq obj (eval obj)))
  (let ((pair (assoc slot obj)))
    (if pair (cdr pair)
           (let ((p (cdr (assoc 'parent obj))))
             (and p (value p slot))))))

; Update a slot in an object
(defun update (obj slot value)
  (when (symbolp obj) (setq obj (eval obj)))
  (let ((pair (assoc slot obj)))
    (when pair (setf (cdr pair) value))))

; Functions needed in uLisp

(defun member (x lst)
  (cond
   ((null lst) nil)
   ((eq x (car lst)) lst)
   (t (member x (cdr lst)))))

(defun remove (x lst)
  (cond
   ((null lst) nil)
   ((eq x (car lst)) (cdr lst))
   (t (cons (car lst) (remove x (cdr lst))))))
