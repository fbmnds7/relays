(setq example
  '("body"
    ("h1" "Welcome")
    ("p" "This is " ("i" "very") " exciting.")))

;; Here’s a slightly simpler version of your function:

(defun list->xml (m)
  (cond
   ((atom m) (princ-to-string (eval m)))
   (t (concatenate 'string
       "<" (first m) ">"
       (apply #'concatenate 'string (mapcar #'list->xml (cdr m)))
       "</" (first m) ">"))))

;; It works!
(list->xml example)
;; "<body><h1>Welcome</h1><p>This is <i>very</i> exciting.</p></body>"
