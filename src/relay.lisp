
(defvar mode 4)
;; 1: single relay on GPIO0
;; 4: four relays on GPIO13/12/14/16

(defun gpio (&rest x)
  (mapc (lambda (p)
          (pinmode p t)
          (digitalwrite p 0))
        x))

(defvar p1 (object nil '(p 13 v 0)))
(defvar p2 (object nil '(p 12 v 0)))
(defvar p3 (object nil '(p 14 v 0)))
(defvar p4 (object nil '(p 16 v 0)))

(when (= mode 1)
  (setf p1 (object nil '(p 0 v 0))))

(defvar ts-hbeat (millis))

(defun println (x s) (princ x s) (princ #\return s) (princ #\newline s))

(defun reply ()
  (cond ((= mode 1)
         (concatenate 'string
                      "{ \"r1\" : " (format nil "~a" (value p1 'v)) " }"))
        ((= mode 4)
         (concatenate 'string
                      "{ \"r1\" : " (format nil "~a" (value p1 'v)) ","
                      "  \"r2\" : " (format nil "~a" (value p2 'v)) ","
                      "  \"r3\" : " (format nil "~a" (value p3 'v)) ","
                      "  \"r4\" : " (format nil "~a" (value p4 'v)) " }"))
        (t "")))

(defun pos (s p)
  (cond 
    ((= 0 (length p)) nil)
    ((< (length s) (length p)) nil)
    ((string= (subseq s 0 (length p)) p) t)
    (t (pos (subseq s 1) p))))

(defun req (s)
  (let ((r "")
        (ts (millis)))
    (loop
      (when (> (millis) (+ 2500 ts)) (return))
      (if (= 0 (available s))
          (delay 500)
          (let ((line (read-line s)))
            (setq r (concatenate 'string r line)))))
    r))

(defun r200 (r s)
  (when r
    (println "HTTP/1.1 200 OK" s)
    (println "Access-Control-Allow-Origin: *" s)
    (println "Content-Type: application/json" s)
    (println "Content-Encoding: text" s)
    (println "Connection: close" s)
    (let ((len_r (+ 2 (length r))))
      (princ "Content-Length: " s)
      (println len_r s) 
      (println "" s)
      (println r s))))

(defun r400 (s)
  (println "HTTP/1.1 400 Bad Request" s)
  (println "Connection: close" s))

(defun toggle-pin (r pat pin)
  (when (pos r pat)
      (let ((sta-v (1+ (* -1 (value pin 'v)))))
        (digitalwrite (value pin 'p) sta-v)
        (update pin 'v sta-v)
        (reply))))

(defun webpage (s)
  (let ((r (req s)))
    ;; (println r nil)
    (setf ts-hbeat (millis))
    (cond ((r200 (toggle-pin r "/r1" 'p1) s))
          ((r200 (toggle-pin r "/r2" 'p2) s))
          ((r200 (toggle-pin r "/r3" 'p3) s))
          ((r200 (toggle-pin r "/r4" 'p4) s))
          ((pos r "/?") (r200 (reply) s))
          (t (r400 s)))))

(defun run-webpage ()
  (setf ts-hbeat (millis))
  (loop
    (when (> (millis) (+ 660000 ts-hbeat))
        (println (wifi-connect ssid pw) nil)
        (setf ts-hbeat (millis)))
    (with-client (s)
      (webpage s)
      (delay 1000))))

(defun init ()
  (when (= mode 1) (gpio 0))
  (when (= mode 4) (gpio 13 12 14 16))
  (wifi-connect ssid pw)
  (wifi-server)
  (run-webpage))

(init)


