
(defvar port 4000)

(defun ip (lis)
  (let ((x 0))
    (mapc (lambda (n) (setq x (+ (ash x 8) n))) (reverse lis))
    x))

(defun lisp-server (&rest iplist)
  (with-client (s (ip iplist) port)
    (print "Listening...")
    (loop
     (unless (= 0 (available s))
       (let ((line (read-line s)))
         (print line)
         (princ (eval (read-from-string line)) s)
         (princ #\return s) 
         (princ #\newline s)))
     (delay 1000))))
